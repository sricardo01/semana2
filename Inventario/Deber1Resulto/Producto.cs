﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deber1Resulto
{
    class Producto
    {

        // Atributos
        private string nombre;
        private string tipo;
        private double proteina;
        private double vitamina;
        private DateTime fechaRegistro;


        // Metodos


        public void validarAtributosGenerales(Producto producto)
        {
            if (producto.Proteina < 1)
            {
                throw new System.ArgumentException("El valor de la proteina no debe ser menor a 1", "proteina");
            }
            if (producto.Vitamina < 1)
            {
                throw new System.ArgumentException("El valor de la vitamina no debe ser menor a 1", "vitamina");
            }
            if (String.IsNullOrEmpty(producto.Tipo))
            {
                throw new System.ArgumentException("Se debe especificar un tipo de producto", "tipo");
            }
        }



        public String reemplazarString(String cadena, string caracterAreemplazar, string caracterNuevo)
        {
            //cadena --> 50,5
            // caracterAreemplazar --> ,
            // nuevo .
            // return 50.5
            return cadena.Replace(caracterAreemplazar, caracterNuevo);
        }


        public virtual String obtenerDescripcionProducto()
        {
            return "El/La " + nombre + "tiene " + proteina + " g proteina y " + vitamina + " g vitamina";
        }

        public virtual String devolverNombre()
        {
            return "El nombre del producto es: " + nombre;
        }
        // Gets and Sets


        public String Nombre {

            get { return nombre; }
            set { nombre = value; }
        }

        public String Tipo {
            get { return tipo; }
            set { tipo = value; }
        }


        public double Proteina
        {

            get { return proteina; }
            set { proteina = value; }
        }

        public double Vitamina
        {

            get { return vitamina; }
            set { vitamina = value; }
        }

        public DateTime FechaRegistro {
            get { return fechaRegistro; }
            set { fechaRegistro = value; }
        }

    }
}
