﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deber1Resulto
{
    class Legumbre: Producto
    {

        // Atributos
        private double glucidos;

        // Metodos 

        public String formarInstruccionInsert(Legumbre legumbre) {

            // Validaciones
            if (legumbre.Glucidos < 1)
            {
                throw new System.ArgumentException("El valor de la los glusidos no debe ser menor a 1", "glosidos");
            }
            validarAtributosGenerales(legumbre);
            // Logica
            string glusidos = reemplazarString(legumbre.Glucidos.ToString(), ",", ".");
            string proteina = reemplazarString(legumbre.Proteina.ToString(), ",", ".");
            string vitamina = reemplazarString(legumbre.Vitamina.ToString(), ",", ".");

            return "INSERT INTO producto(nombre, tipo, proteina,vitamina,glusidos) VALUES" +
                "('" + legumbre.Nombre + "', '"+legumbre.Tipo+"', "+proteina+", "+vitamina+", "+glusidos+")";

        }







        // Sobreescitura;

        public override String obtenerDescripcionProducto()
        {
            return "La legumbre: " + Nombre + " tiene " + glucidos + " g de glucidos, ademas esta compueta de " + Proteina + "g de proteina " 
                +  " y " + Vitamina + " g vitaminas";
        }


        public override string devolverNombre()
        {
            return "";
        }

        // get and set

        public double Glucidos {
            get { return glucidos; }
            set { glucidos = value; }
        }
    }
}
