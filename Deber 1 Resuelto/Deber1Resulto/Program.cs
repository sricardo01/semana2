﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deber1Resulto
{
    class Program
    {
        static void Main(string[] args)
        {

            // Instanciar
            /* Producto carneRes = new Producto();
             carneRes.Nombre = "Carne de Res";
             Console.WriteLine(carneRes.devolverNombre());
             Console.ReadKey();*/

            Console.WriteLine("---------------CARNES-------------------");
            Carne carneDeRes = new Carne();
            carneDeRes.Nombre = "Carne de Res tipo 1";
            carneDeRes.Proteina = 50.5;
            carneDeRes.Vitamina = 10;
            Console.WriteLine(carneDeRes.obtenerDescripcionProducto());
            Console.ReadKey();


            Carne carneDeResDos = new Carne();
            carneDeResDos.Nombre = "Carne de Res tipo 2";
            carneDeResDos.Proteina = 1000;
            carneDeResDos.Vitamina = 5.45;
            Console.WriteLine(carneDeResDos.obtenerDescripcionProducto());
            Console.ReadKey();

            Console.WriteLine("---------------LEGUMBRE-------------------");


            Legumbre legumbre1 = new Legumbre();
            legumbre1.Nombre = "LEGUNBRE 1";
            legumbre1.Glucidos = 50;
            legumbre1.Vitamina = 100;
            legumbre1.Proteina = 60;

            Console.WriteLine(legumbre1.obtenerDescripcionProducto());
            Console.ReadKey();


            Console.WriteLine("---------------MANTECA-------------------");
            Manteca manteca1 = new Manteca();
            manteca1.Nombre = "Manteca de tipo 1";
            manteca1.Lipidos = 50.5;
            manteca1.Proteina = 100;
            manteca1.Vitamina = 10;
            Console.WriteLine(manteca1.obtenerDescripcionProducto());
            Console.ReadKey();



        }
    }
}
